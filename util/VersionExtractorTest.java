package de.christophbrill.appframeworktesting.util;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.util.VersionExtractor;
import io.quarkus.test.junit.QuarkusTest;
import java.time.LocalDateTime;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

@QuarkusTest
class VersionExtractorTest {

    @ConfigProperty(name = "quarkus.application.name")
    String artifactId;

    @Test
    void testGetGitRevision() {
        String mavenVersion = VersionExtractor.getGitVersion("de.christophbrill.appframework", artifactId);
        assertThat(mavenVersion).matches("[0-9a-f]{7}");
    }

    @Test
    void testGetBuildTimestamp() {
        LocalDateTime buildTimestamp = VersionExtractor.getBuildTimestamp("de.christophbrill.appframework", artifactId);
        assertThat(buildTimestamp).isNotNull();
    }
}

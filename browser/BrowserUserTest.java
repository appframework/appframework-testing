package de.christophbrill.appframeworktesting.browser;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;
import static org.assertj.core.api.Assertions.assertThat;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import io.quarkiverse.quinoa.testing.QuinoaTestProfiles;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(QuinoaTestProfiles.Enable.class)
class BrowserUserTest extends AbstractBrowserTest {

    private void assertAllUsersExist(Page page) {
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Tom Tester").setExact(true))).hasCount(1);
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("XXX").setExact(true))).hasCount(1);
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("YYY").setExact(true))).hasCount(1);
    }

    private void assertUsersVisibleInOrder(Page page, String... roleNames) {
        Locator locator = page.locator("xpath=//table[@id='data-list']/tbody/tr/td/a");
        assertThat(locator).hasCount(roleNames.length);
        for (int i = 0; i < roleNames.length; i++) {
            assertThat(locator.nth(i).textContent()).isEqualTo(roleNames[i]);
        }
    }

    @Test
    void listUsers() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_users", "Users");

            // then: the site tells us two roles exist
            assertAllUsersExist(page);

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "Tom Tester", "XXX", "YYY");

            logout(page);
        }
    }

    @Test
    void filterUsers() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_users", "Users");

            // then: the site tells us two roles exist
            assertAllUsersExist(page);

            // given: an input field to search exists
            Locator siteSearch = page.locator("#site_search");
            assertThat(siteSearch).isInViewport();

            // when: we search for 'Admin'
            siteSearch.fill("Tom");

            // then: we only get the role 'Tom Tester' listed
            assertUsersVisibleInOrder(page, "Tom Tester");

            // when: we clear the search field
            siteSearch.clear();

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "Tom Tester", "XXX", "YYY");

            logout(page);
        }
    }

    @Test
    void sortUsers() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_users", "Users");

            Locator sortNameElement = page.locator("#sort_name");
            Locator sortByName = page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Name"))
                    .filter(new Locator.FilterOptions().setHas(sortNameElement));
            Locator sortedByName = sortByName
                    .locator(".sortorder");

            Locator sortLoginElement = page.locator("#sort_login");
            Locator sortByLogin = page
                    .getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Login"))
                    .filter(new Locator.FilterOptions().setHas(sortLoginElement));
            Locator sortedByLogin = sortByLogin
                    .locator(".sortorder");

            // then: we are sorted by name by default
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder");
            assertThat(sortedByLogin).not().isInViewport();

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "Tom Tester", "XXX", "YYY");

            // when: we click sort by name
            sortByName.click();

            // then: we are still sorted by name
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder reverse");
            assertThat(sortedByLogin).not().isInViewport();

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "YYY", "XXX", "Tom Tester");

            // when: we click sort by name
            sortByName.click();

            // then: we are still sorted by name
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder");
            assertThat(sortedByLogin).not().isInViewport();

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "Tom Tester", "XXX", "YYY");

            // when: we click sort by login
            sortByLogin.click();

            // then: we are still sorted by name
            assertThat(sortedByName).not().isInViewport();
            assertThat(sortedByLogin).isInViewport();
            assertThat(sortedByLogin).hasClass("sortorder");

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "Tom Tester", "XXX", "YYY");

            // when: we click sort by login
            sortByLogin.click();

            // then: we are still sorted by name
            assertThat(sortedByName).not().isInViewport();
            assertThat(sortedByLogin).isInViewport();
            assertThat(sortedByLogin).hasClass("sortorder reverse");

            // then: We find the roles in the alphabetical order
            assertUsersVisibleInOrder(page, "YYY", "XXX", "Tom Tester");

            logout(page);
        }
    }

}

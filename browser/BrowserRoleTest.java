package de.christophbrill.appframeworktesting.browser;

import static org.assertj.core.api.Assertions.assertThat;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;
import io.quarkiverse.quinoa.testing.QuinoaTestProfiles;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

@QuarkusTest
@TestProfile(QuinoaTestProfiles.Enable.class)
class BrowserRoleTest extends AbstractBrowserTest {

    private void assertAllRolesExist(Page page) {
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Administrators").setExact(true))).hasCount(1);
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("No Permissions").setExact(true))).hasCount(1);
        assertThat(page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Users").setExact(true))).hasCount(1);
    }

    private void assertRolesVisibleInOrder(Page page, String... roleNames) {
        Locator locator = page.locator("xpath=//table[@id='data-list']/tbody/tr/td/a");
        assertThat(locator).hasCount(roleNames.length);
        for (int i = 0; i < roleNames.length; i++) {
            assertThat(locator.nth(i).textContent()).isEqualTo(roleNames[i]);
        }
    }

    @Test
    void listRoles() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_roles", "Roles");

            // then: the site tells us two roles exist
            assertAllRolesExist(page);

            // then: We find the roles in the alphabetical order
            assertRolesVisibleInOrder(page, "Administrators", "No Permissions", "Users");

            logout(page);
        }
    }

    @Test
    void filterRoles() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_roles", "Roles");

            // then: the site tells us two roles exist
            assertAllRolesExist(page);

            // given: an input field to search exists
            Locator siteSearch = page.locator("#site_search");
            assertThat(siteSearch).isInViewport();

            // when: we search for 'Admin'
            siteSearch.fill("Admin");

            // then: we only get the role 'Administrators' listed
            assertRolesVisibleInOrder(page, "Administrators");

            // when: we clear the search field
            siteSearch.clear();

            // then: We find the roles in the alphabetical order
            assertRolesVisibleInOrder(page, "Administrators", "No Permissions", "Users");

            logout(page);
        }
    }

    @Test
    void sortRoles() {
        try (Page page = login()) {

            // when: we navigate to the roles administration
            clickNavigationAndCheck(page, "menu_roles", "Roles");

            Locator sortNameElement = page.locator("#sort_name");
            Locator sortedByName = page.getByRole(AriaRole.CELL, new Page.GetByRoleOptions().setName("Name")).filter(new Locator.FilterOptions().setHas(sortNameElement)).locator(".sortorder");

            // then: we are sorted by name by default
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder");

            // then: We find the roles in the alphabetical order
            assertRolesVisibleInOrder(page, "Administrators", "No Permissions", "Users");

            // when: we click sort by name
            sortNameElement.click();

            // then: we are still sorted by name
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder reverse");

            // then: We find the roles in the alphabetical order
            assertRolesVisibleInOrder(page, "Users", "No Permissions", "Administrators");

            // when: we click sort by name
            sortNameElement.click();

            // then: we are still sorted by name
            assertThat(sortedByName).isInViewport();
            assertThat(sortedByName).hasClass("sortorder");

            // then: We find the roles in the alphabetical order
            assertRolesVisibleInOrder(page, "Administrators", "No Permissions", "Users");

            logout(page);
        }
    }

}

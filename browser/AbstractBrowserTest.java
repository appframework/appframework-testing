package de.christophbrill.appframeworktesting.browser;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.AriaRole;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public abstract class AbstractBrowserTest {

    protected static Playwright playwright;
    protected static Browser browser;
    protected static BrowserContext browserContext;

    @ConfigProperty(name = "quarkus.http.test-port")
    Integer port;

    @BeforeAll
    static void beforeAll() {
        playwright = Playwright.create();
        BrowserType firefox = playwright
                .firefox();
        if ("true".equals(System.getenv("CI"))) {
            browser = firefox
                    .launch();
        } else {
            browser = firefox
                    .launch(new BrowserType.LaunchOptions().setHeadless(false));
        }
        browserContext = browser.newContext(new Browser.NewContextOptions().setLocale("en"));
    }

    @AfterAll
    static void afterAll() {
        if (browserContext != null) {
            browserContext.close();
        }
        if (browser != null) {
            browser.close();
        }
        if (playwright != null) {
            playwright.close();
        }
    }

    protected final void clickNavigationAndCheck(Page page, String elementToClick, String textToBePresent) {

        // given: an unfolded administration menu
        page.locator("#navbarAdministration").click();

        // given: a clickable element
        Locator locator = page.locator("#" + elementToClick);

        // when: we click the element
        locator.click();

        // then: the router navigates us to our target, and we find the text expected
        Locator locator1 = page.getByRole(AriaRole.HEADING, new Page.GetByRoleOptions().setName(textToBePresent));
        assertThat(locator1).hasCount(1);
    }

    protected final Page login() {
        return login("tom", "tester");
    }

    protected final Page login(String username, String password) {
        Page page = browserContext.newPage();
        page.navigate("http://localhost:" + port);
        page.getByLabel("Username").fill(username);
        page.getByLabel("Password").fill(password);
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Login")).click();
        return page;
    }

    protected final void logout(Page page) {
        page.locator("#menu_logout").click();
    }
}

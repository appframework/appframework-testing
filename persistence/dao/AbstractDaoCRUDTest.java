package de.christophbrill.appframeworktesting.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;

import de.christophbrill.appframework.persistence.model.DbObject;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestSecurity(user = "tom", roles = { "admin" })
public abstract class AbstractDaoCRUDTest<T extends DbObject> {

    protected abstract T createFixture();

    protected abstract void modifyFixture(T fixture);

    protected abstract T findById(Long id);

    protected abstract Long count();

    @Test
    @Transactional
    public void testCRUD() {
        Long count = count();

        // when: we create a fixture
        var fixture = createFixture();
        fixture.persistAndFlush();
        // then: we get back a fixture with an ID
        Long id = fixture.id;
        assertThat(id).isNotNull();

        try {
            // then: we get back a fixture with an ID
            assertThat(fixture.id).isNotNull();

            // when: we modify a fixture
            modifyFixture(fixture);

            // then: we get an updated fixture that has the same ID
            fixture.persist();
            assertThat(fixture.id).isNotNull();
            assertThat(fixture.id).isEqualTo(id);

            // when: we load a fixture
            var loaded = findById(id);
            // then: we get back the fixture matching the ID
            assertThat(loaded.id).isEqualTo(id);
        } finally {
            // when: we remove a fixture
            fixture.delete();

            // then: we can no longer find the fixture
            assertNull(findById(id));
        }

        assertThat(count).isEqualTo(count());
    }
}

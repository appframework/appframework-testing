package de.christophbrill.appframeworktesting.persistence.dao;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
class RoleDaoTest extends AbstractDaoCRUDTest<RoleEntity> {

    @Override
    protected RoleEntity createFixture() {
        RoleEntity fixture = new RoleEntity();
        fixture.name = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected void modifyFixture(RoleEntity fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected RoleEntity findById(Long id) {
        return RoleEntity.findById(id);
    }

    @Override
    protected Long count() {
        return RoleEntity.count();
    }

    @Test
    @Transactional
    void testFindByUserLogin() {
        var roles = RoleEntity.findByUserLogin("tom").list();
        assertThat(roles)
                .isNotNull()
                .hasSize(1);
    }
}

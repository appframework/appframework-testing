package de.christophbrill.appframeworktesting.persistence.dao;

import de.christophbrill.appframework.persistence.model.UserEntity;
import io.quarkus.test.junit.QuarkusTest;
import java.util.UUID;

@QuarkusTest
class UserDaoTest extends AbstractDaoCRUDTest<UserEntity> {

    @Override
    protected UserEntity createFixture() {
        UserEntity fixture = new UserEntity();
        fixture.name = UUID.randomUUID().toString();
        fixture.email = UUID.randomUUID().toString();
        fixture.login = UUID.randomUUID().toString();
        fixture.password = "51cff3c1f0bc59f6187e7040cc12a4e9b1eca7aa";
        return fixture;
    }

    @Override
    protected void modifyFixture(UserEntity fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected UserEntity findById(Long id) {
        return UserEntity.findById(id);
    }

    @Override
    protected Long count() {
        return UserEntity.count();
    }
}

package de.christophbrill.appframeworktesting.persistence.model;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import jakarta.transaction.Transactional;
import java.util.UUID;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestSecurity(user = "tom")
class DbObjectListenerTest {

    @Test
    @Transactional
    void testFieldsGetSet() {
        RoleEntity role = new RoleEntity();
        role.name = UUID.randomUUID().toString();
        role.persistAndFlush();
        assertThat(role.isPersistent()).isTrue();
        assertThat(role.created).isNotNull();
        assertThat(role.modified).isNotNull();
        assertThat(role.createdBy).isNotNull();
        assertThat(role.modifiedBy).isNotNull();
    }
}

package de.christophbrill.appframeworktesting.persistence.model;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.persistence.model.DbObject;
import org.junit.jupiter.api.Test;

class DbObjectTest {

    @Test
    void testToString_withId() {
        DbObject object = new DbObject();
        object.id = 1L;

        assertThat(object).hasToString("DbObject@1");
    }

    @Test
    void testToString_withoutId() {
        DbObject object = new DbObject();

        assertThat(object.toString()).startsWith("DbObject#");
        assertThat(object.toString()).endsWith(Integer.toString(object.hashCode()));
    }

    @Test
    void testEquals_null() {
        DbObject object = new DbObject();

        assertThat(object).isNotEqualTo(null);
    }

    @Test
    void testEquals_differentClasses() {
        DbObject object1 = new DbObject();
        Object object2 = new Object();

        assertThat(object1).isNotEqualTo(object2);
    }

    @Test
    void testEquals_newObjects() {
        DbObject object1 = new DbObject();
        DbObject object2 = new DbObject();

        assertThat(object1).isNotEqualTo(object2);
    }

    @Test
    void testEquals_sameObject() {
        DbObject object = new DbObject();

        assertThat(object).isEqualTo(object);
    }

    @Test
    void testEquals_newObjectsWithSameId() {
        DbObject object1 = new DbObject();
        object1.id = 1L;
        DbObject object2 = new DbObject();
        object2.id = 1L;

        assertThat(object1).isEqualTo(object2);
    }

    @Test
    void testEquals_newObjectsWithDifferentId() {
        DbObject object1 = new DbObject();
        object1.id = 0L;
        DbObject object2 = new DbObject();
        object2.id = 1L;

        assertThat(object1).isNotEqualTo(object2);
    }

    @Test
    void testEquals_newObjectsOneWithId() {
        DbObject object1 = new DbObject();
        object1.id = 1L;
        DbObject object2 = new DbObject();

        assertThat(object1).isNotEqualTo(object2);
    }

    @Test
    void testHashCode_newObjects() {
        DbObject object1 = new DbObject();
        DbObject object2 = new DbObject();

        assertThat(object1.hashCode()).isNotEqualTo(object2.hashCode());
    }

    @Test
    void testHashCode_sameObject() {
        DbObject object = new DbObject();

        assertThat(object).hasSameHashCodeAs(object);
    }

    @Test
    void testHashCode_newObjectsWithSameId() {
        DbObject object1 = new DbObject();
        object1.id = 1L;
        DbObject object2 = new DbObject();
        object2.id = 1L;

        assertThat(object1).hasSameHashCodeAs(object2);
    }

    @Test
    void testHashCode_newObjectsWithDifferentId() {
        DbObject object1 = new DbObject();
        object1.id = 0L;
        DbObject object2 = new DbObject();
        object2.id = 1L;

        assertThat(object1).doesNotHaveSameHashCodeAs(object2);
    }

    @Test
    void testHashCode_newObjectsOneWithId() {
        DbObject object1 = new DbObject();
        object1.id = 1L;
        DbObject object2 = new DbObject();

        assertThat(object1).doesNotHaveSameHashCodeAs(object2);
    }
}

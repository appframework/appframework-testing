package de.christophbrill.appframeworktesting.persistence.selector;

import de.christophbrill.appframework.persistence.model.UserEntity;
import de.christophbrill.appframework.persistence.selector.UserSelector;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class UserSelectorTest extends AbstractResourceSelectorTest<UserEntity> {

    @Override
    protected UserSelector getSelector() {
        return new UserSelector(em);
    }
}

package de.christophbrill.appframeworktesting.persistence.selector;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.persistence.model.DbObject;
import de.christophbrill.appframework.persistence.selector.AbstractResourceSelector;
import io.quarkus.test.security.TestSecurity;

import java.util.UUID;
import org.junit.jupiter.api.Test;

@TestSecurity(user = "tom", roles = { "admin" })
public abstract class AbstractResourceSelectorTest<T extends DbObject> extends AbstractSelectorTest<T> {

    @Override
    protected abstract AbstractResourceSelector<T> getSelector();

    @Test
    public void testSearch() {
        long max = getSelector().count();

        assertThat(getSelector().withSearch(null).count()).isEqualTo(max);
        assertThat(getSelector().withSearch("").count()).isEqualTo(max);

        assertThat(getSelector().withSearch(UUID.randomUUID().toString()).count()).isZero();
    }
}

package de.christophbrill.appframeworktesting.persistence.selector;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assumptions.*;

import de.christophbrill.appframework.persistence.selector.AbstractSelector;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import java.util.List;
import org.junit.jupiter.api.Test;

@QuarkusTest
public abstract class AbstractSelectorTest<T> {

    @Inject
    protected EntityManager em;

    protected abstract AbstractSelector<T> getSelector();

    @Test
    public void testFindAll() {
        AbstractSelector<T> selector = getSelector();
        List<T> entities = selector.findAll();

        int size = selector.count().intValue();
        assertThat(entities).hasSize(size);
    }

    @Test
    public void testOffsetAndLimit() {
        int max = getSelector().count().intValue();

        assumeTrue(max >= 3);

        assertThat(getSelector().findAll()).hasSize(max);
        assertThat(getSelector().withOffset(1).findAll()).hasSize(max - 1);
        assertThat(getSelector().withOffset(max).findAll()).isEmpty();

        assertThat(getSelector().withLimit(2).findAll()).hasSize(2);
        assertThat(getSelector().withOffset(max - 2).findAll()).hasSize(2);
        assertThat(getSelector().withOffset(max - 1).findAll()).hasSize(1);
        assertThat(getSelector().withOffset(max).findAll()).isEmpty();
    }
}

package de.christophbrill.appframeworktesting.persistence.selector;

import de.christophbrill.appframework.persistence.model.RoleEntity;
import de.christophbrill.appframework.persistence.selector.RoleSelector;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class RoleSelectorTest extends AbstractResourceSelectorTest<RoleEntity> {

    @Override
    protected RoleSelector getSelector() {
        return new RoleSelector(em);
    }
}

package de.christophbrill.appframeworktesting.ui;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.ui.dto.VersionInformation;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class VersionInfoServiceTest extends AbstractUiTest {

    @Test
    public void test() {
        VersionInformation versionInformation = given()
            .when()
            .get("/rest/version_info")
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getObject(".", VersionInformation.class);

        assertThat(versionInformation.maven).isNotNull();
        assertThat(versionInformation.git).isNotNull();
    }
}

package de.christophbrill.appframeworktesting.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.ui.dto.Role;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import java.util.UUID;

@QuarkusTest
@TestSecurity(user = "tom", roles = { "ADMIN_ROLES", "SHOW_ROLES" })
public class RoleCRUDTest extends AbstractCRUDTest<Role> {

    @Override
    protected Role createFixture() {
        Role fixture = new Role();
        fixture.name = UUID.randomUUID().toString();
        return fixture;
    }

    @Override
    protected String getPath() {
        return "role";
    }

    @Override
    protected Class<Role> getFixtureClass() {
        return Role.class;
    }

    @Override
    protected void modifyFixture(Role fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(Role fixture, Role created) {
        assertThat(created.name).isEqualTo(fixture.name);
    }
}

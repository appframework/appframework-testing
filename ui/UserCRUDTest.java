package de.christophbrill.appframeworktesting.ui;

import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.ui.dto.User;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import java.util.UUID;

@QuarkusTest
@TestSecurity(user = "tom", roles = { "ADMIN_USERS", "SHOW_USERS" })
public class UserCRUDTest extends AbstractCRUDTest<User> {

    @Override
    protected User createFixture() {
        User fixture = new User();
        fixture.name = UUID.randomUUID().toString();
        fixture.login = UUID.randomUUID().toString();
        fixture.email = UUID.randomUUID().toString();
        fixture.password = "0000000000111111111122222222223333333333";
        return fixture;
    }

    @Override
    protected String getPath() {
        return "user";
    }

    @Override
    protected Class<User> getFixtureClass() {
        return User.class;
    }

    @Override
    protected void modifyFixture(User fixture) {
        fixture.name = UUID.randomUUID().toString();
    }

    @Override
    protected void compareDtos(User fixture, User created) {
        assertThat(created.name).isEqualTo(fixture.name);
        assertThat(created.login).isEqualTo(fixture.login);
        assertThat(created.email).isEqualTo(fixture.email);
        assertThat(created.password).isNull();
        assertThat(fixture.password).isEqualTo("0000000000111111111122222222223333333333");
    }
}

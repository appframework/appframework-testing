package de.christophbrill.appframeworktesting.ui;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import de.christophbrill.appframework.ui.dto.AbstractDto;
import de.christophbrill.appframework.ui.dto.Problem;
import de.christophbrill.appframework.ui.exceptions.NotFoundException;
import io.quarkus.logging.Log;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.junit.jupiter.api.Test;

public abstract class AbstractCRUDTest<T extends AbstractDto> extends AbstractUiTest {

    protected abstract T createFixture();

    protected abstract String getPath();

    protected abstract Class<T> getFixtureClass();

    protected abstract void compareDtos(T fixture, T created);

    protected abstract void modifyFixture(T fixture);

    protected String username = "tom";
    protected String password = "ab4d8d2a5f480a137067da17100271cd176607a1"; // tester

    static {
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Test
    public void testCRUD() {
        String path = "/rest/" + getPath();

        Log.tracev("Testing {0}", path);

        // Create an fixture fixture
        T fixture = createFixture();

        // Read the amount of existing stored elements
        List<T> list = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", getFixtureClass());
        final int preSize = list.size();

        Log.tracev("There are {0} for {1} before the test", preSize, path);

        // when: we create a fixture
        Response post = given()
                .auth()
                .preemptive()
                .basic(username, password)
                .contentType(ContentType.JSON)
                .when()
                .body(fixture)
                .post(path);
        if (post.statusCode() == 400) {
            var problem = post.as(Problem.class);
            throw new AssertionError(problem.title + ": " + problem.detail);
        }
        assertThat(post.statusCode()).isEqualTo(200);
        T created = post
            .as(getFixtureClass());

        // then: we should get the created object
        Long id = created.id;
        assertThat(created.id).isNotNull();
        Log.tracev("Created a new {0} with id {1}", path, id);
        compareDtos(fixture, created);

        // and then: we should have one more stored element
        list = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", getFixtureClass());
        assertThat(list).hasSize(preSize + 1);
        Log.tracev("Now we have {0} for {1}", list.size(), path);

        // when: we load the object by id
        T loaded = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path + "/" + id)
            .as(getFixtureClass());

        // then: we get back the identical object that was just created
        compareDtos(fixture, loaded);

        Log.tracev("We could load the entity {0} for {1}", id, path);

        // Modify our fixture
        fixture.id = id;
        modifyFixture(fixture);

        // when: we update the fixture
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(fixture)
            .put(path + "/" + id);

        Log.tracev("We updated the entity {0} for {1}", id, path);

        // then: we should have an updated fixture
        T updated = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path + "/" + id)
            .as(getFixtureClass());
        compareDtos(fixture, updated);

        Log.tracev("We could retrieve the updated entity {0} for {1}", id, path);

        // and then: we should still have one more stored element
        list = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", getFixtureClass());
        assertThat(list).hasSize(preSize + 1);

        // when: we delete the fixture
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .delete(path + "/" + id);

        Log.tracev("We could delete entity {0} for {1}", id, path);

        // then: we can no longer load the fixture
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path + "/" + id)
            .then()
            .statusCode(204);

        Log.tracev("We can no longer load entity {0} for {1}", id, path);

        // and then: we should have the same amount of more stored elements as before
        // the test
        list = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body()
            .jsonPath()
            .getList(".", getFixtureClass());
        assertThat(list).hasSize(preSize);

        // when: we try to delete the removed entity again
        Response response = given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .delete(path + "/" + id);

        // then: we get an HTTP 400
        assertThat(response.getStatusCode()).isEqualTo(400);

        Log.tracev("We can no longer delete the entity {0} for {1}", id, path);

        // when: we try to update the deleted fixture
        try {
            given()
                .auth()
                .preemptive()
                .basic(username, password)
                .contentType(ContentType.JSON)
                .when()
                .body(fixture)
                .put(path + "/" + id);
        } catch (NotFoundException e) {
            Log.tracev("We can no longer load the entity {0} for {1}", id, path);
            // then: we get a HTTP 404
        }
    }

    protected T emptyFixture()
        throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        return getFixtureClass().getDeclaredConstructor().newInstance();
    }

    @Test
    public void testEmptyUpdate()
        throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        String path = "/rest/" + getPath();

        // when: we try to update the fixture to nothing
        T empty = emptyFixture();
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(empty)
            .put(path + "/" + 12345)
            .then()
            .statusCode(400);
    }

    @Test
    public void testNullCreate() {
        String path = "/rest/" + getPath();

        // Create an fixture fixture
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .post(path)
            .then()
            .statusCode(400);
    }

    @Test
    public void testCreateWithId() {
        String path = "/rest/" + getPath();

        // Create an fixture fixture
        T fixture = createFixture();
        fixture.id = 12345L;
        given()
            .auth()
            .preemptive()
            .basic(username, password)
            .contentType(ContentType.JSON)
            .when()
            .body(fixture)
            .post(path)
            .then()
            .statusCode(400);
    }
}

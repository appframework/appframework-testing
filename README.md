# Common tests for appframework

This repository holds common tests and infrastructure for creating tests for
appframework based applications.

## Usage

You only need to add two sections to your pom.xml file. First you will add
the dependency:

    <dependency>
        <groupId>de.christophbrill.appframework</groupId>
        <artifactId>appframework-testing</artifactId>
        <version>1.0.6</version>
        <scope>test</scope>
    </dependency>

You also can add the section to your pom.xml to run the tests provided by this
package (maven will otherwise ignore the wonderful tests in this package):

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.20</version>
                <configuration>
                    <dependenciesToScan>
                        <dependency>de.christophbrill.appframework:appframework-testing</dependency>
                    </dependenciesToScan>
                </configuration>
            </plugin>
        </plugins>
    </build>

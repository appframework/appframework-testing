package de.christophbrill.appframeworktesting;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;

class MessagesTest {

    @Test
    void testDeAndEnMatch() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> messagesEn;
        try (
            InputStream messageFile =
                this.getClass().getResourceAsStream("/src/main/webapp/app/scripts/i18n/en.json")
        ) {
            assumeThat(messageFile).isNotNull();
            messagesEn = mapper.readValue(messageFile, new TypeReference<>() {});
        }

        Map<String, String> messagesDe;
        try (
            InputStream messageFile =
                this.getClass().getResourceAsStream("/src/main/webapp/app/scripts/i18n/de.json")
        ) {
            assumeThat(messageFile).isNotNull();
            messagesDe = mapper.readValue(messageFile, new TypeReference<>() {});
        }

        Set<String> messagesEnKeySet = messagesEn.keySet();
        Set<String> messagesDeKeySet = messagesDe.keySet();
        assertThat(messagesEnKeySet).hasSize(messagesDeKeySet.size());

        assertThat(messagesEnKeySet).containsAll(messagesDeKeySet);
        assertThat(messagesDeKeySet).containsAll(messagesEnKeySet);
    }
}
